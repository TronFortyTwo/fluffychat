import QtQuick 2.9
import Ubuntu.Components 1.3
import "../components"

// Initial Walkthrough tutorial

Page {
    id: walkthroughPage
    anchors.fill: parent

    header: PageHeader {
        title: ""
        StyleHints {
            dividerColor: "#00000000"
            backgroundColor: "#00000000"
        }
    }

    Walkthrough {
        id: walkthrough
        anchors.fill: parent

        appName: "Zafluffy"

        onFinished: {
            walkthrough.visible = false
            mainLayout.walkthroughFinished = true
            mainLayout.updateInfosFinished = version
            init ()
        }

        model: [


        Component {
            id: slide1

            Item {
                id: slide1Container

				
                Image {
                    id: smileImage
                    anchors {
                        top: slide1Container.top
                        topMargin: units.gu(4)
                        horizontalCenter: slide1Container.horizontalCenter
                    }
                    //height: (parent.height - introductionText.height - bodyText.contentHeight - 4.5*units.gu(4))
					height: units.gu(36)
					width: units.gu(36)
                    //fillMode: Image.PreserveAspectFit
                    source: Qt.resolvedUrl("../../assets/info-logo.png")
					sourceSize.width: width
					sourceSize.height: height
                    asynchronous: true
                }

                Label {
                    id: introductionText
                    anchors {
                        bottom: bodyText.top
                        bottomMargin: units.gu(4)
                    }
                    elide: Text.ElideRight
                    fontSize: "x-large"
                    horizontalAlignment: Text.AlignHLeft
                    maximumLineCount: 2
                    text: i18n.tr("Welcome!")
                    width: units.gu(36)
                    wrapMode: Text.WordWrap
                }

                Label {
                    id: bodyText
                    anchors {
                        bottom: slide1Container.bottom
                        bottomMargin: units.gu(10)
                    }
                    fontSize: "large"
                    height: contentHeight
                    horizontalAlignment: Text.AlignHLeft
                    text: i18n.tr("Chat inside the Matrix network. This app is a FluffyChat fork with a professional look and feel. Many thanks to the FluffyChat team!")
                    width: units.gu(36)
                    wrapMode: Text.WordWrap
                }
            }
        },


        Component {
            id: slide2

            Item {
                id: slide2Container

                Label {
                    id: introductionText
                    anchors {
                        top: slide2Container.top
                        topMargin: units.gu(15)
                    }
                    elide: Text.ElideRight
                    fontSize: "x-large"
                    horizontalAlignment: Text.AlignHLeft
                    maximumLineCount: 2
                    text: i18n.tr("Join the Matrix")
                    width: units.gu(36)
                    wrapMode: Text.WordWrap
                }

                Label {
                    id: bodyText
                    anchors {
                        bottom: slide2Container.bottom
                        bottomMargin: units.gu(15)
                    }
                    fontSize: "large"
                    height: contentHeight
                    horizontalAlignment: Text.AlignHLeft
                    text: i18n.tr("Zafluffy is compatible with other messengers like FluffyChat, Riot, Fractal or uMatriks. You can also join IRC channels, participate in XMPP chats and bridge Telegram groups.")
                    width: units.gu(36)
                    wrapMode: Text.WordWrap
                }
            }
        },


        Component {
            id: slide3

            Item {
                id: slide3Container

                Label {
                    id: introductionText
                    anchors {
                        top: slide3Container.top
                        topMargin: units.gu(15)
                    }
                    elide: Text.ElideRight
                    fontSize: "x-large"
                    horizontalAlignment: Text.AlignHLeft
                    maximumLineCount: 2
                    text: i18n.tr("What's new in version %1?").arg(version)
                    width: units.gu(36)
                    wrapMode: Text.WordWrap
                }

                Label {
                    id: bodyText
                    readonly property string template1: i18n.tr("Minor bugfixes and updated translations.")
                    readonly property string template2: i18n.tr("Critical bug fixed.")
                    readonly property string template3: i18n.tr("Security bug fixed.")
                    readonly property string template4: i18n.tr("Updated translations. Thanks to all translators.")
                    anchors {
                        bottom: slide3Container.bottom
                        bottomMargin: units.gu(15)
                    }
                    fontSize: "large"
                    height: contentHeight
                    horizontalAlignment: Text.AlignHLeft
                    text: template1
                    width: units.gu(36)
                    wrapMode: Text.WordWrap
                }
            }
        },



        Component {
            id: slide4
            Item {
                id: slide4Container

                Label {
                    id: introductionText
                    anchors {
                        top: slide4Container.top
                        topMargin: units.gu(15)
                    }
                    elide: Text.ElideRight
                    fontSize: "x-large"
                    horizontalAlignment: Text.AlignHLeft
                    maximumLineCount: 2
                    text: i18n.tr("Community funded")
                    width: units.gu(36)
                    wrapMode: Text.WordWrap
                }

                Label {
                    id: finalMessage
                    anchors {
                        bottom: continueButton.top
                        bottomMargin: units.gu(7)
                    }
                    fontSize: "large"
                    horizontalAlignment: Text.AlignHLeft
                    text: i18n.tr("Zafluffy is open and nonprofit. The development and servers is all community funded. Donate to the upstream FluffyChat on <a href='https://ko-fi.com/krille'>Ko-fi</a> or <a href='https://liberapay.com/KrilleChritzelius/donate'>Liberapay</a>")
                    width: units.gu(36)
                    wrapMode: Text.WordWrap
                    linkColor: mainLayout.brightMainColor
                    textFormat: Text.StyledText
                    onLinkActivated: contentHub.openUrlExternally ( link )
                }

                Button {
                    id: continueButton
                    anchors {
                        bottom: slide4Container.bottom
                        bottomMargin: units.gu(10)
                        horizontalCenter: slide4Container.horizontalCenter
                    }
                    color: UbuntuColors.green
                    height: units.gu(5)
                    text: i18n.tr("Continue")
                    width: units.gu(36)

                    onClicked: walkthrough.finished()
                }
            }
        }
        ]
    }
}
